let mongoose = require('mongoose');
let GroceryItem = require('./models/GroceryItem.js');

mongoose.connection.db.dropDatabase();

var initial = [{
	name: 'KUDA',
	damage: '4/10',
	range: '6/10',
	fireRate: '5/10',
	accuracy: '4/10',
	url: 'https://itzdarkvoid.files.wordpress.com/2015/06/gunsmith.png'
},{
	name: 'VMP',
	damage: '7/10',
	range: '3/10',
	fireRate: '8/10',
	accuracy: '2/10',
	url: 'http://i1.wp.com/www.codwatch.com/wp-content/uploads/2015/10/Black-Ops-3-VMP-SMG.jpg'
},{
	name:'WEEVIL',
	damage: '3/10',
	range: '4/10',
	fireRate: '8/10',
	accuracy: '4/10',
	url: 'http://i0.wp.com/www.codwatch.com/wp-content/uploads/2015/10/Black-Ops-3-Weevil-SMG.jpg'
},{
	name:'VESPER',
	damage: '4/10',
	range: '3/10',
	fireRate: '9/10',
	accuracy: '4/10',
	url: 'https://i.ytimg.com/vi/MJSi-1Y9mZQ/maxresdefault.jpg'
}];

initial.forEach(function(item){
	new GroceryItem(item).save();
});
