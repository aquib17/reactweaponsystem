//"use strict";
var mongoose = require('mongoose');

var GroceryItemSchema = {
	name:String,
	damage: String,
	range: String,
	fireRate: String,
	accuracy: String,
	url: String
};

var GroceryItem = mongoose.model('GroceryItem',GroceryItemSchema,'groceryItems');

module.exports = GroceryItem;
