"use strict";

let GroceryItem = require('./GroceryItem.jsx'),
	GroceryListAddItem = require('./GroceryListAddItem.jsx'),
	GrocerySingleItem = require('./GrocerySingleItem.jsx'),
    Router = require('react-router').Router,
    Route = require('react-router').Route,
    Link = require('react-router').Link,
	React = require('react/addons');

module.exports = React.createClass({
	getInitialState: function(){
		return {
			Wmenu : true,WaddMenu: true,WSmenu: true
		}
	},

	render:function(){
		return (
			<div>
			<ul className="listCSS">
			<li ><Link to="/ADDITEM"><button className="button-primary">
			ADD ITEM</button></Link></li>
				</ul>
				<div >

				{this.props.items.map((item,index)=>{
					return (
						<div >
						<div>
						<GroceryItem item={item} key={"item"+index} />
						</div>
						</div>
					)
				})}
				</div>

                 <div>
				 {this.props.children}
				 </div>
				
			</div>
		)

		
	}


})
