var groceryAction = require("./../stores/GroceryItemActionCreator.jsx");
let React = require('react/addons');

module.exports = React.createClass({
	getInitialState: function(){
		return {
			Wname:'', Wdamage: '',Wrange: '', Waccuracy: '', WfireRate:'',Wurl:''
		}
	},
	addItem:function(e){
		e.preventDefault();

		groceryAction.add({
			name:	this.state.Wname,
			damage: this.state.Wdamage,
			range: this.state.Wrange,
			fireRate: this.state.WfireRate,
			accuracy: this.state.Waccuracy,
			url: this.state.Wurl
		});


     this.props.onItemSubmit({Wmenu: true});


		this.setState({
			Wname:'', Wdamage: '',Wrange: '', Waccuracy: '', WfireRate:'',Wurl:''
		})

	},
	handleInputName:function(e){
		this.setState({Wname : e.target.value})
	},
	handleInputdamage:function(e){
		if(e.target.value<=10&&e.target.value>=0){
		this.setState({Wdamage : e.target.value+'/10'})
		}
		else{

			alert("give rating between zero and ten");
			e.target.value='';
		}
	},
	handleInputrange:function(e){
		if(e.target.value<=10&&e.target.value>=0){
		this.setState({Wrange : e.target.value+'/10'});
		}
		else{

			alert("give rating between zero and ten");
			e.target.value='';
		}
		
	},
	handleInputaccuracy:function(e){
		if(e.target.value<=10&&e.target.value>=0){
		this.setState({Waccuracy : e.target.value+'/10'})
		}
		else{

			alert("give rating between zero and ten");
			e.target.value='';
		}
		
	},
		handleInputUrl:function(e){
		this.setState({Wurl : e.target.value})
	},
	handleInputfireRate:function(e){
		if(e.target.value<=10&&e.target.value>=0){
		this.setState({WfireRate : e.target.value+'/10'})
		}
		else{

			alert("give rating between zero and ten");
			e.target.value='';
		}
		
	},
	render:function(){
		return (
		 <div className="grocery-addItem">
			<form  onSubmit={this.addItem}>
			<h2><b>ADD NEW WEAPON</b></h2>
			<br></br>
					<label className="six columns">NAME</label><input
						type="text"
						name="Wname"
						placeholder="Weapon Name"
						className="six columns"
						required
						value={this.state.Wname}
						onChange={this.handleInputName}
					/>
					<br></br>
					
					<label className="six columns">DAMAGE</label><input
						type="text"
						name="Wdamage"
						className="six columns"
						placeholder="Give rating out of 10"
						required
						value={this.state.Wdamage}
						onChange={this.handleInputdamage}
					/>
					<br></br>
					<label className="six columns">RANGE</label><input
						type="text"
						name="Wrange"
						placeholder="Give rating out of 10"
						className="six columns"
						required
						value={this.state.Wrange}
						onChange={this.handleInputrange}
					/>
					<br></br>
					<label className="six columns">ACCURACY</label><input
						type="text"
						name="Waccuracy"
						placeholder="Give rating out of 10"
						className="six columns"
						required
						value={this.state.Waccuracy}
						onChange={this.handleInputaccuracy}
					/>
					<br></br>
					<label className="six columns">FIRE RATE</label><input
						type="text"
						placeholder="Give rating out of 10"
						name="WfireRate"
						className="six columns"
						required
						value={this.state.WfireRate}
						onChange={this.handleInputfireRate}
					/>
					<br></br>
					<label className="six columns">WEAPON IMAGE</label><input
						type="text"
						placeholder="Give IMAGE URL"
						name="WfireRate"
						className="six columns"
						required
						value={this.state.Wurl}
						onChange={this.handleInputUrl}
					/>
					<br></br>
				<button className="six columns button-primary">Add a new weapon to the list</button>
			</form>
			</div>
		)
	}
})
