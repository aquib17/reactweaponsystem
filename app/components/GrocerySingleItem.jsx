var dispatcher = require("./../dispatcher.js");
var groceryAction = require("./../stores/GroceryItemActionCreator.jsx");
var React = require('react/addons');
var cx = React.addons.classSet;

module.exports = React.createClass({

	togglePurchased:function(e){
		e.preventDefault();

		if (!this.props.item.purchased){
			groceryAction.buy(this.props.item);
		} else {
			groceryAction.unbuy(this.props.item);
		}
	},
	delete:function(e){
		e.preventDefault();
		groceryAction.delete(this.props.item);
	},
	render:function(){
		return (
			<div className="grocery-item row">
			<div className="three columns">
					<img className="picCSS" src={this.props.item.url}></img>
					
				</div>
				<div className="three columns">
					<h4>
						{this.props.item.name}
					</h4>
				</div>
				<div className="four columns">
					<h4>DAMAGE: {this.props.item.damage}</h4>
					<h4>RANGE: {this.props.item.range}</h4>
					<h4>FIRE RATE: {this.props.item.fireRate}</h4>
					<h4> ACCURACY: {this.props.item.accuracy}</h4>
					
				</div>
			
			</div>
		)
	}
})